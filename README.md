# Archivos de configuración de instancias de AWS

En este directorio se encuentran los **archivos de configuración** de las instancias de AWS que se utilizaron para el proyecto. Por lo tanto, estos archivos requieren de unas modificaciones para que puedan ser utilizados en otras instancias, siendo más especificos, en el **nombre DNS de la IP privada** de la instancia. Dichas instancias estan configuradas de la siguiente manera:

<br>
<img src="./images/diagrama_arquitectura.png" width="400" height="400" style="display: block; margin-left: auto; margin-right: auto;">
<br>

## Archivo Docker Compose

El archivo [docker-compose.yml](docker-compose.yml) es el archivo que se utilizó para la creación de los contenedores de Docker en las instancias de los **servidores web** de AWS, los cuales estan ejecutando cada uno dos contenedores con el backend del proyecto (mismo que se encuentra en el repositorio https://gitlab.com/Poncianiix/todolist), y un contenedor **NGINX** para el balanceo de carga entre estos.

```dockerfile
services: 
  web-app-1:
    build: .
    image: todo-list:1.0
    restart: always
    ports: 
      - 3000:3000
    environment:
      - PORT=3000
      - MONGO=mongodb://<NOMBRE_DNS_DE_IP_PRIVADA>:27017/todo-list

  web-app-2:
    build: .
    image: todo-list:1.0
    restart: always
    ports: 
      - 3001:3001
    environment:
      - PORT=3001
      - MONGO=mongodb://<NOMBRE_DNS_DE_IP_PRIVADA>:27017/todo-list
```

En este caso debemos cambiar el **nombre DNS de la IP privada** de la instancia en la variable de entorno **MONGO** de cada contenedor, para que apunte a la instancia que contiene la base de datos **MongoDB**.

## Archivo de configuración de NGINX

El archivo [nginx.conf](nginx.conf) es el archivo de configuración de NGINX que se utilizó para el balanceo de carga entre los dos servidores web de AWS.

```nginx
upstream app {
		server <NOMBRE_DNS_DE_IP_PRIVADA>:80;
		server <NOMBRE_DNS_DE_IP_PRIVADA>:80;
	}
```

En este caso debemos cambiar el **nombre DNS de la IP privada** de cada instancia, para que apunte a cada uno de los servidores web de AWS.

## Archivo Hosts

El archivo [hosts](hosts) es el archivo de configuración de los **hosts** de las instancias de AWS, el cual se utilizó para que nuestro servidor bastión pueda acceder a las instancias de los servidores web de AWS.

```python
mongo ansible_host=<NOMBRE_DNS_DE_IP_PRIVADA> ansible_user=ec2-user ansible_port=22 ansible_ssh_private_key_file=~/ssh-keys/mongo-key.pem
web-server-1 ansible_host=<NOMBRE_DNS_DE_IP_PRIVADA> ansible_user=ec2-user ansible_port=22 ansible_ssh_private_key_file=~/ssh-keys/web-server-key.pem
web-server-2 ansible_host=<NOMBRE_DNS_DE_IP_PRIVADA> ansible_user=ec2-user ansible_port=22 ansible_ssh_private_key_file=~/ssh-keys/web-server-key.pem
load-balancer ansible_host=<NOMBRE_DNS_DE_IP_PRIVADA> ansible_user=ec2-user ansible_port=22 ansible_ssh_private_key_file=~/ssh-keys/load-balancer-key.pem
```

En este caso debemos cambiar el **nombre DNS de la IP privada** de cada instancia, para que apunte a cada una de las instancias de AWS. Asimismo, debemos cambiar la **ruta de la llave privada** de cada instancia, para que apunte a la llave privada de cada instancia. 

## Playbook Ansible

El archivo [playbook.yml](playbook.yml) es el archivo de configuración de Ansible. Este se ejecuta en el servidor bastión, tomando en cuenta que nuestro archivo [hosts](hosts) ya se encuentre correctamente configurado en el servidor bastión y que los archivos [docker-compose.yml](docker-compose.yml) y [nginx.conf](nginx.conf) se encuentren en el servidor bastión, ya que los transifere a las demás instancias de AWS.

Para este caso, no debemos de cambiar nada en realidad, ya que el archivo de configuración de Ansible se encarga de hacer las operaciones necesarias para la creación de los contenedores de Docker en las instancias de los **servidores web** de AWS, la configuración de la instancia con **base de datos** MongoDB de AWS y la instalación de NGINX en la instancia del **balanceador de carga** de AWS.

## S3 Bucket

En este proyecto tambien se incluye un **bucket** de S3 de AWS, el cual se utilizó para alojar una página web estática que se encuentra en el repositorio https://gitlab.com/a338817/todofront. Esta página web estática se creo con el framework de **ReactJS** y se aloja en el bucket de S3 de AWS, para que los usuarios puedan acceder a ella desde cualquier dispositivo conectado a internet y por ende, poder utilizar la aplicación web del **TODO List**.
